package battnavale.framework;

import battnavale.controller.ConsoleGameController;
import battnavale.controller.GUIGameController;
import battnavale.controller.GameController;
import battnavale.view.ConsoleView;
import battnavale.view.GUIView;

public class IOBuilder {
	private IOBuilder() {}
	
	public static GameController buildIOController(String type) throws IOControllerNotFoundException {
		switch (type) {
		case "GUI":
			GUIGameController guiController = new GUIGameController();
			GUIView guiView = new GUIView(guiController);
			guiController.setView(guiView);
			guiView.setVisible(true);
			return guiController;
		case "Console":
			ConsoleGameController consoleController = new ConsoleGameController();
			ConsoleView consoleView = new ConsoleView();
			consoleView.setIOListener(consoleController);
			consoleView.start();
			return consoleController;
		default:
			throw new IOControllerNotFoundException(type);
		}
	}
}
