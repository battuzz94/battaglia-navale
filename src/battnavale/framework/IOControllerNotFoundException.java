package battnavale.framework;

public class IOControllerNotFoundException extends Exception {
	final String description;
	public IOControllerNotFoundException(String s) {
		this.description = s;
	}
	
	public String getDescription() {
		return "Could not find a controller named " + description;
	}
}
