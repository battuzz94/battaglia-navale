package battnavale.view;

import battnavale.model.State;

public interface View {
	public void update(State s);
}
