package battnavale.view;

import java.util.Scanner;

import battnavale.common.ConsoleIOListener;

public class ConsoleView {
	ConsoleIOListener listener;
	
	public ConsoleView() {
		this.listener = null;
	}
	
	public void setIOListener(ConsoleIOListener listener) {
		this.listener = listener;
	}
	
	public void start() {
		startView();
	}
	
	private void startView() {
		Thread t = new Thread(new Runnable() {
			public void run() {
				Scanner in = new Scanner(System.in);
				String command;
				while (true) {
					command  = in.nextLine();
					if (listener != null)
						listener.handleInput(command);
				}
				
			}
		});
		t.start();
	}
	
	public void update(int r, int c) {
		System.out.println(r + " " + c);
	}
	
	public void notifyBadInput() {
		System.out.println("Input non valido");
	}
}
