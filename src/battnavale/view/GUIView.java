package battnavale.view;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import battnavale.common.Costanti;
import battnavale.common.GameState;
import battnavale.controller.GUIGameController;
import battnavale.controller.GameController;
import battnavale.model.Coordinata;
import battnavale.model.State;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.GridLayout;


public class GUIView extends JFrame implements View {

	private JPanel contentPane;
	private transient GUIGameController controller;
	private JButton[][] buttonsLeft;
	private JButton[][] buttonsRight;

	/**
	 * Create the frame.
	 */
	public GUIView(GUIGameController c) {
		this.controller = c;
		buttonsLeft = new JButton[Costanti.DIM][Costanti.DIM];
		buttonsRight = new JButton[Costanti.DIM][Costanti.DIM];
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 700, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		JLabel lblBattagliaNavale = new JLabel("Battaglia Navale!");
		lblBattagliaNavale.setHorizontalAlignment(SwingConstants.CENTER);
		lblBattagliaNavale.setFont(new Font("Lucida Grande", Font.PLAIN, 25));
		contentPane.add(lblBattagliaNavale, BorderLayout.NORTH);
		
		JPanel parentPanel = new JPanel();
		contentPane.add(parentPanel, BorderLayout.CENTER);
		parentPanel.setLayout(new GridLayout(0, 2, 60, 0));
		
		JPanel leftPanel = new JPanel();
		parentPanel.add(leftPanel);
		leftPanel.setLayout(new GridLayout(Costanti.DIM, Costanti.DIM));
		
		JPanel rightPanel = new JPanel();
		parentPanel.add(rightPanel);
		rightPanel.setLayout(new GridLayout(Costanti.DIM, Costanti.DIM));
		for (int i = 0; i < Costanti.DIM; i++) {
			for (int j = 0; j < Costanti.DIM; j++) {
				buttonsLeft[i][j] = new JButton("~");
				buttonsRight[i][j] = new JButton("~");
				
				buttonsLeft[i][j].setFocusable(false);
				buttonsRight[i][j].setFocusable(false);
				
				final int x = i;
				final int y = j;

				buttonsLeft[i][j].addActionListener(
					e -> controller.gridTouchEvent(new Coordinata(x, y))
				);
				
				
				
				leftPanel.add(buttonsLeft[i][j]);
				
				buttonsRight[i][j].addActionListener(
					e -> controller.gridTouchEvent(new Coordinata(-x-1, -y-1))
				);
				
				
				rightPanel.add(buttonsRight[i][j]);
			}
		}
		
	}

	@Override
	public void update(State state) {
		for (int i = 0; i < Costanti.DIM; i++) {
			for (int j = 0; j < Costanti.DIM; j++) {
				buttonsRight[i][j].setText(state.getBoardOfPlayer(GameController.PLAYER0).getCellState(i, j));
				buttonsLeft[i][j].setText(state.getBoardOfPlayer(GameController.PLAYER1).getCellState(i, j));
			}
		}
		
		if (state.getState() == GameState.GAME_FINISHED) {
			JOptionPane.showMessageDialog(this, "Il gioco è finito!\n Ha vinto il giocatore: " + state.getWinner());
		}
	}

}
