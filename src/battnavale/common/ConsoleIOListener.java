package battnavale.common;

public interface ConsoleIOListener {
	public void handleInput(String input);
}
