package battnavale.common;

import battnavale.model.Coordinata;

public interface GUIIOListener {
	public void gridTouchEvent(Coordinata c);
}
