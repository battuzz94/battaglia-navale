package battnavale.common;

import java.util.Random;

public enum Direzione {
	SU(0, 1), DESTRA(1,0), GIU(0, -1), SINISTRA(-1, 0);
	
	int direzioneX;
	int direzioneY;
	
	private Direzione(int x, int y) {
		direzioneX = x;
		direzioneY = y;
	}
	
	public int dx() {
		return direzioneX;
	}
	public int dy() {
		return direzioneY;
	}
	
	public static Direzione generaDirezioneCasuale() {
		Random random = new Random();
		int choice = random.nextInt(4);
		switch(choice) {
			case 0:
				return SU;
			case 1:
				return GIU;
			case 2:
				return DESTRA;
			default:
				return SINISTRA;
		}
	}
}

