package battnavale.model;

import java.util.Random;

import battnavale.common.Costanti;

public class Coordinata implements Cloneable {
	int x;
	int y;
	
	public Coordinata(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	@Override
	public boolean equals(Object c) {
		if (c instanceof Coordinata)
			return ((Coordinata)c).x == x && ((Coordinata)c).y == y;
		else
			return false;
	}
	
	@Override
	public Coordinata clone() {
		return new Coordinata(x, y);
	}
	
	public static Coordinata generaCoordinataRandom() {
		int boardSize = Costanti.DIM;
		Random random = new Random();
		int y = random.nextInt(boardSize);
		int x = random.nextInt(boardSize);
		return new Coordinata(x, y);
	}
	
	@Override
	public String toString() {
		return "(" + x + " " + y + ")";
	}
}
