package battnavale.model;

import battnavale.common.Direzione;

public class Nave {
	public Nave(int dim, Coordinata coord, Direzione dir) {
		this.dim = dim;
		coordinate = new Coordinata[dim];
		for (int i = 0; i < dim; i++) 
			coordinate[i] = new Coordinata(coord.x + i * dir.dx(), coord.y + i*dir.dy());
		
		colpite = new boolean[dim];
		for (int i = 0; i < dim; i++)
			colpite[i] = false;
		
		this.direzione = dir;
	}
	
	public void hit(Coordinata c) {
		for (int i = 0; i < coordinate.length; i++) {
			if (c.equals(coordinate[i]))
				colpite[i] = true;
		}
	}
	
	public Coordinata[] getCoordinate() {
		return coordinate.clone();
	}
	
	public Coordinata getCoordinata() {
		return coordinate[0].clone();
	}
	
	public Direzione getDirezione() {
		return direzione;
	}
	
	public boolean isDown() {
		boolean down = true;
		for (int i = 0; i < colpite.length && down; i++) 
			if (colpite[i] == false)
				down = false;
		
		return down;
	}
	
	public int getSize() {
		return dim;
	}
	
	private Coordinata[] coordinate;
	private boolean[] colpite;
	int dim;
	Direzione direzione;
}
