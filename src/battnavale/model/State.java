package battnavale.model;

import battnavale.common.GameState;

public class State implements Cloneable {
	GameState gameState;
	Griglia[] g;
	int currentPlayer;
	int otherPlayer;
	int winner = -1;
	
	public State() {
		g = new Griglia[2];
		g[0] = new Griglia();
		g[1] = new Griglia();
		
		currentPlayer = 0;
		otherPlayer = 1;
		
		this.gameState = GameState.GAME_INITIALIZED;
	}
	
	private State(GameState initialState) {
		this.gameState = initialState;
	}
	
	public void setWinner(int player) {
		this.winner = player;
	}
	
	public int getWinner() {
		return winner;
	}
	
	public int getCurrentPlayer() {
		return currentPlayer;
	}
	
	public int getOtherPlayer() {
		return otherPlayer;
	}
	
	public Griglia getCurrentPlayerBoard() {
		return g[currentPlayer];
	}
	
	public Griglia getOtherPlayerBoard() {
		return g[otherPlayer];
	}
	public Griglia getBoardOfPlayer(int player) {
		return g[player];
	}
	
	public void nextTurn() {
		currentPlayer = (currentPlayer + 1) % 2;
		otherPlayer = (otherPlayer + 1) % 2;
	}
	
	
	
	public GameState getState() {
		return gameState;
	}
	
	
	public void changeState(GameState newState) {
		this.gameState = newState;
	}
	
	@Override
	public State clone() {
		return new State(this.gameState);
	}
	
	@Override
	public String toString() {
		return "state: " + gameState + "Griglia1: " + g[0] + "Griglia2: " + g[1] + "curr: " + currentPlayer + "oth: " + otherPlayer;
	}
}
