package battnavale.model;

import java.util.ArrayList;

import battnavale.common.Costanti;
import battnavale.common.Direzione;

public class Griglia implements Cloneable {
	public Griglia() {
		navi = new ArrayList<Nave>(Costanti.NUM_NAVI);
		grigliaSpari = new boolean[Costanti.DIM][Costanti.DIM];
		
		grigliaNavi = new Nave[Costanti.DIM][Costanti.DIM];
	}
	
	public void addBoat(Nave nave){
		navi.add(nave);
		
		Coordinata[] coordinate = nave.getCoordinate();
		for (Coordinata c : coordinate)
			grigliaNavi[c.getX()][c.getY()] = nave;
	}
	
	public String getCellState(int i, int j) {
		if (grigliaSpari[i][j] == true && grigliaNavi[i][j] != null)
			if (grigliaNavi[i][j].isDown())
				return " X ";
			else
				return " # ";
		else if (grigliaSpari[i][j] == true)
			return " O ";
		else
			return " ~ ";
	}
	
	public boolean isValidBoat(Nave n) {
		Direzione d = n.getDirezione();
		Coordinata c = n.getCoordinata();
		
		boolean valid = true;
		
		for (int i = 0; i < n.getSize() && valid; i++) {
			
			int cx = c.x + i * d.dx();
			int cy = c.y + i * d.dy();
			
			if (!inBound(cx, cy) || grigliaNavi[cx][cy] != null)
				valid = false;
		}
		return valid;
	}
	
	private boolean inBound(int x, int y) {
		return (x >= 0 && y >= 0 && x < Costanti.DIM && y < Costanti.DIM);
	}
	
	public boolean allBoatsDown() {
		boolean ans = true;
		int i = 0;
		
		for (i = 0; i < Costanti.DIM; i++)
			for (int j = 0; j < Costanti.DIM; j++)
				if (grigliaNavi[i][j] != null && grigliaSpari[i][j] == false)
					ans = false;
		
		return ans;
	}
	
	public boolean isValid(Coordinata sparo) {
		return inBound(sparo.getX(), sparo.getY()) && grigliaSpari[sparo.x][sparo.y] == false;
	}
	
	public void hit(Coordinata sparo) {
		
		if (grigliaNavi[sparo.getX()][sparo.getY()] != null)
			grigliaNavi[sparo.getX()][sparo.getY()].hit(sparo);
		
		grigliaSpari[sparo.getX()][sparo.getY()] = true;
	}
	
	public void printBoats() {
		for (Nave[] row : grigliaNavi) { 
			for (Nave n : row)
				if (n == null)
					System.out.print(" ~ ");
				else
					System.out.print(" " + n.getSize() + " ");
			System.out.println();
		}
	}
	
	public void printBoard() {
		for (int i = 0; i < Costanti.DIM; i++) {
			for (int j = 0; j < Costanti.DIM; j++) {
				System.out.print(getCellState(i, j));
			}
			System.out.println();
		}
	}
	
	private Nave[][] grigliaNavi;
	private ArrayList<Nave> navi;
	private boolean[][] grigliaSpari;
}
