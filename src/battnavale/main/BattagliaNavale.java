package battnavale.main;

import battnavale.controller.GameController;
import battnavale.framework.IOBuilder;
import battnavale.framework.IOControllerNotFoundException;

public class BattagliaNavale {
	private BattagliaNavale() {}
	
	public static void main(String[] args) {
		
		GameController c = null;
		
		try {
			c = IOBuilder.buildIOController("GUI");
			
			System.out.println("Avvio il gioco..");
			c.startGame();
			
			while (!c.finished())
				try {
					Thread.sleep(5000);
				}
			catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		catch (IOControllerNotFoundException e) {
			e.getDescription();
			e.printStackTrace();
			System.exit(1);
		}
		
	}
}
