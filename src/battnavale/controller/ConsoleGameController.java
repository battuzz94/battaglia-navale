package battnavale.controller;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import battnavale.common.ConsoleIOListener;
import battnavale.common.GameState;
import battnavale.model.Coordinata;

public class ConsoleGameController extends GameController implements ConsoleIOListener {
	
	public ConsoleGameController() {
		super();
	}
	
	@Override
	public void handleInput(String input) {
		if (state.getState() == GameState.GAME_RUNNING) {
			Pattern p = Pattern.compile("^(?<row>\\d+)\\s+(?<col>\\d+)\\s*");  // ^(\d+)\s+(\d+)\s* //
			Matcher m = p.matcher(input);
			
			boolean matched = m.matches();
			
			if (matched) {
				int row = Integer.parseInt(m.group("row"));
				int col = Integer.parseInt(m.group("col"));
				Coordinata c = new Coordinata(row, col);
				handleHit(c);
			}
			else
				System.out.println("Bad input!");
		}
		
	}
	
}
