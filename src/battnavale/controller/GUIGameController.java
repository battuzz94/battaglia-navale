package battnavale.controller;

import battnavale.common.GUIIOListener;
import battnavale.common.GameState;
import battnavale.model.Coordinata;

public class GUIGameController extends GameController implements GUIIOListener {
	
	public GUIGameController() {
		super();
	}

	@Override
	public void gridTouchEvent(Coordinata c) {
		System.out.println("Ho toccato in: " + c);
		if (state.getState() == GameState.GAME_RUNNING) {
			if (state.getCurrentPlayer() == GameController.PLAYER0) {
				if (c.getX() >= 0 && c.getY() >= 0) {
					super.handleHit(c);
				}
			}
			else {
				if (c.getX() < 0 && c.getY() < 0 ) {
					Coordinata c2 = new Coordinata(-c.getX() - 1, -c.getY() - 1);
					super.handleHit(c2);
				}
			}
		}
	}
	
	
	
}
