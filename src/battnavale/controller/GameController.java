package battnavale.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import battnavale.common.Costanti;
import battnavale.common.Direzione;
import battnavale.common.GameState;
import battnavale.model.Coordinata;
import battnavale.model.Nave;
import battnavale.model.State;
import battnavale.view.View;

public abstract class GameController {
	
	public static final int PLAYER0 = 0;
	public static final int PLAYER1 = 1;
	State state;
	
	View view;
	
	
	public GameController() {
		state = new State();
		
		positionateBoats(state.getCurrentPlayer());
		positionateBoats(state.getOtherPlayer());
		
		
		state.changeState(GameState.GAME_INITIALIZED);
		
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		System.out.println(gson.toJson(state));
		
	}
	
	public void setView(View v) {
		this.view = v;
	}
	
	public final void startGame() {
		state.changeState(GameState.GAME_RUNNING);
		System.out.println("Il gioco è avviato!");
		
		if (view != null) {
			view.update(state);
		}
		
		state.getBoardOfPlayer(GameController.PLAYER0).printBoats();
		System.out.println("\n\n");
		state.getBoardOfPlayer(GameController.PLAYER1).printBoats();
		
		System.out.println("\n\n");
		
		state.getOtherPlayerBoard().printBoard();
	}
	
	public final boolean finished() {
		return state.getCurrentPlayerBoard().allBoatsDown() || state.getOtherPlayerBoard().allBoatsDown();
	}
	
	
	protected final void handleHit(Coordinata c) {
		if (state.getOtherPlayerBoard().isValid(c)) {
			state.getOtherPlayerBoard().hit(c);
			
			if (!checkWinner()) {
				nextTurn();
				
				System.out.println("Lo stato è: " + state);
				
				printBoard();
			}
			view.update(state);
		}
		else 
			System.out.println("Coordinata non valida! ");
	}
	
	private boolean checkWinner() {
		if (state.getOtherPlayerBoard().allBoatsDown()) {
			state.changeState(GameState.GAME_FINISHED);
			state.setWinner(state.getCurrentPlayer());

			System.out.println("Complimenti, ha vinto il player " + state.getCurrentPlayer());
			return true;
		}
		return false;
	}
	
	private void nextTurn() {
		state.nextTurn();
	}
	
	private void printBoard() {
		state.getOtherPlayerBoard().printBoard();
	}
	
	private void positionateBoats(int p) {
		for (int i = 1; i <= Costanti.NUM_NAVI; i++) {
			Direzione d = Direzione.generaDirezioneCasuale();
			Coordinata c = Coordinata.generaCoordinataRandom();
			Nave n = new Nave(i, c, d);
			
			while (!state.getBoardOfPlayer(p).isValidBoat(n)) {
				d = Direzione.generaDirezioneCasuale();
				c = Coordinata.generaCoordinataRandom();
				n = new Nave(i, c, d);
			}
			state.getBoardOfPlayer(p).addBoat(n);
		}
	}

}
